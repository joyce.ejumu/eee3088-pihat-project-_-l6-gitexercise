# EEE3088 PiHat Project _ L6 GitExercise

The primary function of the PiHat is to measure its vertical distance under the surface of water. The PiHat design makes use of a pressure sensor to determine the pressure it undergoes at various time intervals. Using the relationship between pressure and depth underwater, the PiHat measures, records and displays its depth up to 150m below the surface of water.

The PiHat design can be broken up into the following three submodules:

- **Power supply subsytem**
The function of the power supply is to convert an Ac input voltage into a DC voltage which is required by the GPIO. In attion to this, the power supply requires a switch that allows the device to have an 'on' and 'off' mode and an LED to indicate the status of the device.

- **Amplifier**
The function of the amplifier is to ensure that the voltage output from the pressure sensor is converted to a voltage that remains between 0 and 3.3V. The output from the amplifier is then input into the GPIO.

- **Status LEDs**
The function of the status LEDs indicate the depth of the device under the surface of the water. At when the device reaches a specific depth, an LED will turn on and remain on.